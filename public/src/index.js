import keyInput from "./keyinput.js";
import * as THREE from "../node_modules/three/build/three.module.js"
import { GLTFLoader } from "../node_modules/three/examples/jsm/loaders/GLTFLoader.js";
import { OrbitControls } from "../node_modules/three/examples/jsm/controls/OrbitControls.js";

// Setup
// Scene
const scene = new THREE.Scene();
scene.background = new THREE.Color(0xa8def0);
// scene.background = new THREE.Color(0x000000);


// Camera
const ratio = window.innerWidth / window.innerHeight;
const camera = new THREE.PerspectiveCamera(75, ratio, 0.1, 1000);
camera.position.set(5, 5, 0);

// Renderer
const renderer = new THREE.WebGLRenderer({ antialias: true });
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.shadowMap.enable = true;
renderer.gammaOuput = true;
renderer.gammaFactor = 2.2;
document.body.appendChild(renderer.domElement);

// Controls
const orbitControls = new OrbitControls(camera, renderer.domElement);
orbitControls.enableDamping = true;
orbitControls.minDistance = 5;
orbitControls.maxDistance = 15;
orbitControls.enablePan = false;
orbitControls.maxPolarAngle = Math.PI / 2 - 0.05;
orbitControls.update();

// Model with animation
var model;
new GLTFLoader().load('../threejs_company_demo/assets/Soldier.glb', function(gltf) {
    model = gltf.scene;
    model.traverse(function(object) {
        if (object.isMesh) {
            object.castShadow = true;
        }
    });
    model.position.set(0, 0, 6)
    scene.add(model);
})

// Control Keys
const keysPressed = {}
document.addEventListener('keydown', (event) => {
    if (event.shiftKey) {

    } else {
        keysPressed[event.key.toLowerCase()] = true;
    }
}, false);
document.addEventListener('keyup', function(event) {
    keysPressed[event.key.toLowerCase()] = false;
}, false);

// Create Light
scene.add(new THREE.AmbientLight(0xffffff, 1))

const dirLight = new THREE.DirectionalLight(0xffffff, 1)
dirLight.position.set(-60, 100, -10);
dirLight.castShadow = true;
dirLight.shadow.camera.top = 50;
dirLight.shadow.camera.bottom = -50;
dirLight.shadow.camera.left = -50;
dirLight.shadow.camera.right = 50;
dirLight.shadow.camera.near = 0.1;
dirLight.shadow.camera.far = 200;
dirLight.shadow.mapSize.width = 4096;
dirLight.shadow.mapSize.height = 4096;
scene.add(dirLight);

// Create ground
// TEXTURES
const textureLoader = new THREE.TextureLoader();
const placeholder = textureLoader.load("../threejs_company_demo/assets/placeholder.png");

const WIDTH = 200
const LENGTH = 200

const geometry = new THREE.PlaneGeometry(WIDTH, LENGTH, 512, 512);
const material = new THREE.MeshBasicMaterial({ map: placeholder })
wrapAndRepeatTexture(material.map)

const floor = new THREE.Mesh(geometry, material)
floor.receiveShadow = true
floor.rotation.x = -Math.PI / 2
scene.add(floor)

for (let i = 0; i < 5; i++) {
    const road = new THREE.TextureLoader();
    const roadPlacehoder = road.load("../threejs_company_demo/assets/stone.jpg");

    const geometryRoad = new THREE.PlaneGeometry(1, 1);
    const materialRoad = new THREE.MeshBasicMaterial({ map: roadPlacehoder })
    wrapAndRepeatTexture(material.map)

    const floorRoad = new THREE.Mesh(geometryRoad, materialRoad)
        // floorRoad.receiveShadow = true
        // floorRoad.rotation.x = -Math.PI / 2
    floorRoad.position.set(2, 0.001, i)
    floorRoad.receiveShadow = true
    floorRoad.rotation.x = -Math.PI / 2
    scene.add(floorRoad)
}

for (let i = -100; i < 100; i++) {
    const road = new THREE.TextureLoader();
    const roadPlacehoder = road.load("../threejs_company_demo/assets/street.jpg");

    const geometryRoad = new THREE.PlaneGeometry(1, 1);
    const materialRoad = new THREE.MeshBasicMaterial({ map: roadPlacehoder })
    wrapAndRepeatTexture(material.map)

    const floorRoad = new THREE.Mesh(geometryRoad, materialRoad)
        // floorRoad.receiveShadow = true
        // floorRoad.rotation.x = -Math.PI / 2
    floorRoad.position.set(i, 0.001, 5)
    floorRoad.receiveShadow = true
    floorRoad.rotation.x = -Math.PI / 2
    scene.add(floorRoad)
}

// Company
const gltfLoader = new GLTFLoader();
var mixerFoutain, mixerHologram;

gltfLoader.load('../threejs_company_demo/assets/module/1.company/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.3, 0.3, 0.3);
    scene.add(loadGLTF);
})

// Machine Drink
gltfLoader.load('../threejs_company_demo/assets/module/2.machine/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.5, 0.5, 0.5);
    loadGLTF.position.set(3, 0, -3)
    scene.add(loadGLTF);
})

// Sign Ads
gltfLoader.load('../threejs_company_demo/assets/module/10.sign/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.005, 0.005, 0.005);
    loadGLTF.rotation.y = -Math.PI / 2
    loadGLTF.position.set(-1, 0, 4)
    scene.add(loadGLTF);
})

// Sign Board Name
gltfLoader.load('../threejs_company_demo/assets/module/12.sign_name/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.1, 0.1, 0.1);
    loadGLTF.rotation.y = 3
    loadGLTF.position.set(-1.8, 2, 3.5)
    scene.add(loadGLTF);
})

// Sign Board
gltfLoader.load('../threejs_company_demo/assets/module/11.sign_board/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.01, 0.01, 0.01);
    loadGLTF.rotation.y = 3
    loadGLTF.position.set(4, 0, 3.5)

    const road = new THREE.TextureLoader();
    const roadPlacehoder = road.load("../threejs_company_demo/assets/logo.png");

    const geometryRoad = new THREE.PlaneGeometry(1, 1);
    const materialRoad = new THREE.MeshBasicMaterial({ map: roadPlacehoder })
    wrapAndRepeatTexture(material.map)

    const floorRoad = new THREE.Mesh(geometryRoad, materialRoad)
        // floorRoad.receiveShadow = true
        // floorRoad.rotation.x = -Math.PI / 2
    floorRoad.scale.set(0.8, 0.8, 0.8)
    floorRoad.position.set(3.5, 0.75, 4.1)
    floorRoad.receiveShadow = true
    floorRoad.rotation.x = -Math.PI / 12
    floorRoad.rotation.y -= 0.2
    floorRoad.rotation.z -= 0.03

    scene.add(floorRoad)

    scene.add(loadGLTF);
})

// Sign Board Name
gltfLoader.load('../threejs_company_demo/assets/module/5.tree3/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.5, 0.5, 0.5);
    loadGLTF.rotation.y = 3
    loadGLTF.position.set(-4, 0, 2.5)
    scene.add(loadGLTF);
})

gltfLoader.load('../threejs_company_demo/assets/module/4.tree2/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.5, 0.5, 0.5);
    loadGLTF.rotation.y = 3
    loadGLTF.position.set(10, 0, 1)
    scene.add(loadGLTF);
})

// Foutain
gltfLoader.load('../threejs_company_demo/assets/module/7.fountain/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.002, 0.002, 0.002);
    loadGLTF.position.set(-6, -0.1, 10)
    scene.add(loadGLTF);

    mixerFoutain = new THREE.AnimationMixer(loadGLTF);

    gltfScene.animations.forEach((clip) => {

        mixerFoutain.clipAction(clip).play();

    });
})

gltfLoader.load('../threejs_company_demo/assets/module/9.hologram_earth/scene.gltf', (gltfScene) => {
    var loadGLTF = gltfScene.scene;
    loadGLTF.scale.set(0.3, 0.3, 0.3);
    loadGLTF.position.set(1, 7, -4)
    scene.add(loadGLTF);

    mixerHologram = new THREE.AnimationMixer(loadGLTF);

    gltfScene.animations.forEach((clip) => {

        mixerHologram.clipAction(clip).play();

    });
})

// for (let i = 0; i < 7; i++) {
//     var num = Math.floor(Math.random() * (3 - 1 + 1) + 1)
//     var link;
//     switch (num) {
//         case 1:
//             link = "../threejs_company_demo/assets/module/3.tree1/scene.gltf"
//             break;
//         case 2:
//             link = "../threejs_company_demo/assets/module/4.tree2/scene.gltf"
//             break;
//         case 3:
//             link = "../threejs_company_demo/assets/module/5.tree3/scene.gltf"
//             break;
//         default:
//             link = "../threejs_company_demo/assets/module/5.tree3/scene.gltf"

//             break;
//     }
//     gltfLoader.load(link, (gltfScene) => {
//         var loadGLTF = gltfScene.scene;
//         loadGLTF.scale.set(1, 1, 1);
//         let px = Math.floor(Math.random() * (50 - (-50) + 1) + (-50))
//         let py = 0;
//         let pz = Math.floor(Math.random() * (50 - (-50) + 1) + (-50))
//         loadGLTF.position.set(px, py, pz)
//         scene.add(loadGLTF);
//     })
// }

// Create function animation
var clock = new THREE.Clock();

function animate() {
    requestAnimationFrame(animate);
    if (keyInput.isPressed(87)) {
        model.position.z -= 0.05;
    }
    if (keyInput.isPressed(83)) {
        model.position.z += 0.05;
    }
    if (keyInput.isPressed(65)) {
        model.position.x += 0.05;
    }
    if (keyInput.isPressed(68)) {
        model.position.x -= 0.05;
    }
    if (keyInput.isPressed(81)) {
        model.position.y += 0.05;
    }
    if (keyInput.isPressed(69)) {
        model.position.y -= 0.05;
    }
    var delta = clock.getDelta();

    if (mixerHologram) mixerHologram.update(delta);
    if (mixerFoutain) mixerFoutain.update(delta);

    camera.lookAt(scene.position);
    renderer.render(scene, camera);
}
animate();

function wrapAndRepeatTexture(map) {
    map.wrapS = map.wrapT = THREE.RepeatWrapping;
    map.repeat.x = map.repeat.y = 10;
}

// Loop for attribute of object
// scene.traverse(function(child) {
//     if (child.class === "item") {
//         child.rotation.x += num;
//         child.rotation.y += num;
//     }
// });